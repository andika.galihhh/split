from django.db import models
from django import forms

# Create your models here.

class kegiatanModel(models.Model):
    kegiatan = models.CharField(max_length = 50)
    
    def __str__(self):
        return "{}. {}".format(self.id,self.kegiatan)

class pesertaModel(models.Model):
    nama_peserta = models.CharField(max_length = 50)
    ikutKegiatan = models.ForeignKey(
    kegiatanModel, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "{}. {}".format(self.id,self.nama_peserta)
