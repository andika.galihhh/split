from django import forms
from .models import kegiatanModel
from .models import pesertaModel
from django.forms import TextInput,Select,Textarea

class kegiatanForm(forms.ModelForm):
    class Meta:
        model = kegiatanModel
        fields = [
            'kegiatan',
            # 'kegiatan2',
            # 'kegiatan3',
        ]

        widgets = {
                'kegiatan': forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder': 'Tulis kegiatan anda'
                    }
                ),
        

        }

class pesertaForm(forms.ModelForm):
    class Meta:
        model = pesertaModel
        fields = [
            'nama_peserta',
        ]

        widgets = {
                'nama_peserta': forms.TextInput(
                    attrs={
                        'class':'form-control',
                        'placeholder': 'Tuliskan nama anda'
                    }
                ),

        }

        # 'kegiatan2': forms.TextInput(
                #     attrs={
                #         'class':'form-control',
                #         'placeholder': 'Nama dosen Pengajar'
                #     }
                # ),
                # 'kegiatan3': forms.TextInput(
                #     attrs={
                #         'class':'form-control',
                #         'placeholder': 'ex. 2.2522'
                #     }
                # ),
               