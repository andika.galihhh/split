from django.urls import path
from . import views

app_name = 'sova'

urlpatterns = [
    path('',views.sova, name='sova'),
    path('form/',views.kegiatanform, name='form'),
    path('hasil/',views.hasil, name='hasil'),
    path('peserta/<int:idpeserta>/',views.pesertaform, name='buat'),
    path('delete/<int:idkegiatan>/',views.delete, name='delete'),
    path('deleteOrang/<int:idpeserta>/',views.deleteOrang, name='deleteOrang'),
]