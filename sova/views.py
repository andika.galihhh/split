from django.shortcuts import render,redirect
from .forms import kegiatanForm, pesertaForm
from .models import kegiatanModel, pesertaModel
from django.http import HttpResponseRedirect

# Create your views here.
def sova(request):
    return render(request,"cypher.html")

def kegiatanform(request):
    kegiatan_form = kegiatanForm(request.POST or None)

    context = {
        'heading' : 'kegiatan Form',
        'form' : kegiatan_form
    }
    
    if request.method == 'POST':

        if kegiatan_form.is_valid():
            kegiatan_form.save()
          
            return redirect('sova:hasil')

    return render(request,'form.html',context)

def pesertaform(request,idpeserta):
    peserta_form = pesertaForm(request.POST or None)

    context = {
        'heading' : 'Form peserta',
        'form' : peserta_form
    }
    
    if request.method == 'POST':

        if peserta_form.is_valid():
            peserta = pesertaModel(ikutKegiatan=kegiatanModel.objects.get(id=idpeserta),nama_peserta=peserta_form.data['nama_peserta'])
            peserta.save()
          
            return redirect('sova:hasil')

    return render(request,'peserta.html',context)

def hasil(request):
    kegiatan = kegiatanModel.objects.all()
    peserta = pesertaModel.objects.all()

    context = {
        'page_title':'list_post',
        'kegiatan':kegiatan,
        'page_title':'list_post_peserta',
        'peserta':peserta,

    }
    return render(request,'hasil.html',context)

def delete(request, idkegiatan):
    kegiatanModel.objects.filter(id=idkegiatan).delete()
    return redirect('sova:hasil')

def deleteOrang(request, idpeserta):
    pesertaModel.objects.filter(id=idpeserta).delete()
    return redirect('sova:hasil')

# def hasil_peserta(request):
#     posts2 = pesertaModel.objects.all()

#     context = {
#         'page_title':'list_post',
#         'posts':posts2,

#     }
#     return render(request,'hasil.html',context)