from django.test import TestCase, Client
from django.urls import resolve
from .views import sova, pesertaform,kegiatanform
from .models import kegiatanModel, pesertaModel 
from .apps import SovaConfig

# Create your tests here.
class TestKegiatan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/') 
        self.assertEqual(response.status_code, 200)# url bisa di load atau tidak

    def test_index_func(self):
        found =  resolve('/')
        self.assertEqual(found.func, sova)# apakah menjalankan fungsi sova

    def test_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'cypher.html')# cek html yang di load cyper atau bukan

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(SovaConfig.name,'sova')# cek app yang jalan sova atau bukan

class TestTambahKegiatan(TestCase):
    def test_create_url_is_exist(self):
        response = Client().get('/form/')
        self.assertEqual(response.status_code, 200)

    def test_create_func(self):
        found = resolve('/form/')
        self.assertEqual(found.func, kegiatanform)

    def test_create_using_template(self):
        response = Client().get('/form/')
        self.assertTemplateUsed(response, 'form.html')

    def test_create_new_kegiatan(self):
        kegiatan = kegiatanModel(kegiatan = "PPW")#dummy object
        kegiatan.save()
        self.assertEqual(kegiatanModel.objects.all().count(),1)# ngecek output yang ada di model sesuai yang di test

    def test_url_kegiatan_is_exist(self):
        response = Client().post('/', data={'kegiatan':'Coding'})# setelah klik lihat kegiatan apakah url nya jalan atau tidak
        self.assertEqual(response.status_code, 200)

class TestPeserta(TestCase) :
    def setUp(self):
        kegiatan = kegiatanModel(kegiatan="PPW")
        kegiatan.save()
        self.assertEqual(kegiatanModel.objects.all().count(),1)

    def test_regris_POST(self):
        response = Client().post('/peserta/1/', data={'nama_peserta':'ag'})# url jalan atau tidak/url redirect, peserta ke post atau tidak
        self.assertEqual(response.status_code, 302)

    def test_regris_GET(self):
        response = self.client.get('/peserta/1/')
        self.assertTemplateUsed(response, 'peserta.html')#ngecek form apakah url nya jalan dan peserta ke get atau tidak
        self.assertEqual(response.status_code, 200)

class TestHapusNama(TestCase):
    def setUp(self):
        acara = kegiatanModel(kegiatan="Coding")
        acara.save()
        nama = pesertaModel(nama_peserta="ag")
        nama.save()

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/deleteOrang/1/')
        self.assertEqual(response.status_code, 302)# ngecek kalo pencet delete memakai metode tsb jalan tidak url nya

    def test_hapus_url_is_exist(self):
        response = self.client.get('/deleteOrang/1/')
        self.assertEqual(response.status_code, 302)

class TestHapusKegiatan(TestCase):
    def setUp(self):
        acara = kegiatanModel(kegiatan="Coding")
        acara.save()
        nama = pesertaModel(nama_peserta="ag")
        nama.save()

    def test_hapus_url_activity_is_exist(self):
        response = Client().post('/delete/1/')
        self.assertEqual(response.status_code, 302)# ngecek kalo pencet delete memakai metode tsb jalan tidak url nya

    def test_hapus_activity_url_is_exist(self):
        response = self.client.get('/delete/1/')
        self.assertEqual(response.status_code, 302)