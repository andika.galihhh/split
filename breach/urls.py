from django.urls import path
from . import views

app_name = 'breach'

urlpatterns = [
    path('',views.breach, name='breach'),
    path('libbox/', views.libbox, name='libbox'),
]