from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponse
import requests
import json


# Create your views here.

def breach(request):
    return render(request,"icebox.html")

def libbox(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    retrive = requests.get(url)
    data = json.loads(retrive.content)
    return JsonResponse(data, safe=False)