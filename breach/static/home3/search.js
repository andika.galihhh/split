
$("#keyword").keyup(function(){
    var input = $("#keyword").val();

    //calling using ajax
    $.ajax({

        //direct url to google api's
        url: '/breach/libbox/?q=' + input,
        
        success: function(data){
            //assign items into array_items
            var array_items = data.items;

            $("#daftar_isi").empty();

            //iterate every items in array_items
            for(i = 0; array_items.length; i++){
                //getting the title of the books
                var title = array_items[i].volumeInfo.title;
                var source_image = array_items[i].volumeInfo.imageLinks.smallThumbnail; 
                var description = array_items[i].volumeInfo.description;
                var read_more = array_items[i].volumeInfo.previewLink;
                var image = "<img src=" + source_image + "class=" + "card-img" + ">"
                $("#daftar_isi").append(
                    "<div class=" + "card mb-3" + "style=" + "max-width: 540px;" + ">" +
                        "<div class=" + "row no-gutters" + ">" +
                            "<div class=" + "col-md-4" + ">" +
                                image +
                            "</div>" +
                            "<div class=" + "col-md-8" + ">" +
                                "<div class=" + "card-body" + ">" +
                                    "<h5 class=" + "card-title" + ">" + title + "</h5>" +
                                    "<p class=" + "card-text" +">" + description + "</p>" +
                                    "<a href=" + read_more + "class=" + "btn btn-primary" + ">" + "Read More" + "</a>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div>"
                );
            }
        }
    });
});