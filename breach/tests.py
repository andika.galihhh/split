
from django.test import TestCase, Client
from django.urls import resolve
from .views import breach
from .apps import BreachConfig

# Create your tests here.
class TestKegiatan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/breach/') 
        self.assertEqual(response.status_code, 200)# url bisa di load atau tidak

    def test_index_func(self):
        found =  resolve('/breach/')
        self.assertEqual(found.func, breach)# apakah menjalankan fungsi sova

    def test_using_template(self):
        response = Client().get('/breach/')
        self.assertTemplateUsed(response,'icebox.html')# cek html yang di load cyper atau bukan

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(BreachConfig.name,'breach')# cek app yang jalan sova atau bukan

class TestSearchEvent(TestCase):
    def test_event_searchbook_url_is_exist(self):
        response = Client().get('/breach/libbox/?q=book')
        self.assertEqual(response.status_code, 200)
