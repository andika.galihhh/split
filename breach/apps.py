from django.apps import AppConfig


class BreachConfig(AppConfig):
    name = 'breach'
