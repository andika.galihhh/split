from django.urls import path

from . import views
urlpatterns = [
    path('', views.brimstone, name='brimstone'),
    path('register/', views.registerhaven, name="register"),
    path('login/', views.loginhaven, name='login'),
    path('logout/', views.logouthaven, name='logout'),
]
