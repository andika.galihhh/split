from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import brimstone,registerhaven,loginhaven,logouthaven
from django.contrib.auth.models import User
from .apps import BrimstoneConfig

# Create your tests here.

class TestTemplate(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/brimstone/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/brimstone/')
        self.assertEqual(found_func.func, brimstone)

    def test_event_using_template(self):
        template = Client().get('/brimstone/')
        self.assertTemplateUsed(template, 'haven.html')

class TestLogIn(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/brimstone/login/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/brimstone/login/')
        self.assertEqual(found_func.func, loginhaven)

    def test_event_using_template(self):
        template = Client().get('/brimstone/login/')
        self.assertTemplateUsed(template, 'loginhaven.html')

    def test_signin_with_new_account(self):
        c = Client()

        user = User.objects.create(
            username='andikagalihhh', email='andikagalihhh@gmail.com')
        user.set_password('andika1234')
        user.save()

        logged_in = c.login(username='andikagalihhh',
                            password='andika1234')

        self.assertTrue(logged_in)


class TestSignUp(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/brimstone/register/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/brimstone/register/')
        self.assertEqual(found_func.func, registerhaven)

    def test_event_using_template(self):
        template = Client().get('/brimstone/register/')
        self.assertTemplateUsed(template, 'registerhaven.html')


class TestLogOut(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/brimstone/logout/')
        self.assertEqual(response.status_code, 302)

    def test_event_func(self):
        found_func = resolve('/brimstone/logout/')
        self.assertEqual(found_func.func, logouthaven)

    
class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(BrimstoneConfig.name,'brimstone')# cek app yang jalan sova atau bukan