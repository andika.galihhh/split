from django.apps import AppConfig


class BrimstoneConfig(AppConfig):
    name = 'brimstone'
