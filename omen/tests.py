from django.test import TestCase, Client
from django.urls import resolve
from .views import omen
from .apps import omenConfig

# Create your tests here.
class TestKegiatan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/omen/') 
        self.assertEqual(response.status_code, 200)# url bisa di load atau tidak

    def test_index_func(self):
        found =  resolve('/omen/')
        self.assertEqual(found.func, omen)# apakah menjalankan fungsi sova

    def test_using_template(self):
        response = Client().get('/omen/')
        self.assertTemplateUsed(response,'ascent.html')# cek html yang di load cyper atau bukan

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(omenConfig.name,'omen')# cek app yang jalan sova atau bukan

