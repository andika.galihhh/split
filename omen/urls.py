from django.urls import path
from . import views

app_name = 'omen'

urlpatterns = [
    path('',views.omen, name='omen'),
]